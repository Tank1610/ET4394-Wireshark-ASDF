# Wireshark Project

Repo for the Wireshark project of group ASDF


### Prerequisites

The script data_process.py uses Python 2.7.

## Running the script

To run the whole script, we can simply run this command in the Linux command line

```
bash script.sh
```

If we want to process separate csv formatted packet captures file, we can run only the Python script

```
python data_process.py [wireshark result file name].csv
```
Where the wireshark result file should be preformatted to contain the Source and Info columns on columns 0 and 4 respectively. In the included datasets, file names ending in "-formatted" are already in the correct format.

To customize the output data that is plotted, several flags and parameters can be changed in the script.

`INCLUDE_OTHERS` can be set to include vendors that appear rarely in the dataset or those whose MAC addresses are found.

`INCLUDE_THRESH` is the minimum number of devices a vendor needs to have in the dataset to be presented in the plot.

`AP_ONLY` can be set to produce a plot with only WLAN Access Point vendors

## Authors

* **Tuan Anh Nguyen** - (https://github.com/tank1610)
* **Haris Suwignyo** - (https://github.com/harissuwig)
