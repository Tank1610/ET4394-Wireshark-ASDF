#!/bin/bash

clear
echo "Wireshark sniffing script."

#Changing the network interface to monitor mode
airmon-ng check kill
airmon-ng start wlan0

#Trap CTRL+C command
trap end_read SIGINT SIGTERM
PIDS=""

#run this when ctrl+c is pressed
function end_read
{
	#saving the data to csv file
	airmon-ng stop wlan0mon
	service network-manager restart
	tshark -r capture.pcapng -T fields -e _ws.col.Source -e _ws.col.Destination -e wlan.ta -e wlan.ra -e _ws.col.Info -E header=y -E separator=, -E quote=d -E occurrence=f > capture.csv
	python data_process.py capture.csv
}

echo "Capturing packets... Press Ctrl+C to stop capturing packets"

#Capturing packets
tshark -i wlan0mon -F pcapng -w capture.pcapng &
PIDS=$!
wait $PIDS
