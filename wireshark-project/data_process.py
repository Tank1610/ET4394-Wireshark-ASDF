#python script to process wireshark data and provide insight
#into vendor types (and chipsets) of Wi-Fi access points in multiple
#locations across TU-Delft campus

#Run as "python data_process.py [wireshark result file name].csv"

import sys
import csv
import matplotlib.pyplot as plt
import requests
import json
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import time


#Flags
INCLUDE_OTHERS = 0  #Set to 1 to include devices without vendor name and vendors that appeared rarely
INCLUDE_THRESH = 1  #Minimum number of devices for a vendor to be excluded from group "Others"
AP_ONLY = 1         #Set to 1 to only get AP vendors



packet_list=[]      #list of packets from csv
vendor_list = []    #list of all vendors from csv
device_list = []    #list of all devices from csv
mac_list = []       #list of mac addresses
vendor_packet_count = dict()   #a dict to keep track of how packets received from each vendor
device_packet_count = dict()   #a dict to keep a count of how many packets received from device

device_per_vendor = dict()
vendor_dist_dataset = list()

unique_device_list = list()
unique_vendor_list = list()


url = "http://api.macvendors.com/"



#Read wireshark csv file
def read_csv(file_name):
    try:
        data_file = open(file_name,'r')
        datareader = csv.reader(data_file, delimiter=',')
        for row in datareader:
            packet_list.append(row)
    except Exception as e:
        print("File not found or not a csv.")
    return 0

#Helper to make plots look nicer
def make_autopct(values):
    def my_autopct(pct):
        total = sum(values)
        val = int(round(pct*total/100.0))
        return '{p:.2f}%  ({v:d})'.format(p=pct,v=val)
    return my_autopct


#API call to get vendor name from MAC address
#Cannot be used on large dataset due to limits from the API provider
def get_vendor_name_api(mac_address):
    response = requests.get(url+str(mac_address))
    return response.content


#Extract list of vendors and devices from MAC addresses
def get_vendor_list(packet_list):

    if not AP_ONLY:
    ### For getting vendors of all devices  
        for row in packet_list[1:]:
            if "(" not in row[0]:
                device_list.append(str(row[0])[:])
            else:
                device_list.append(str(row[0][:-28]))

            device_id = str(row[0])
            try:
                vend_name, junk = device_id.split("_")
            except ValueError as e:
                vend_name = str(row[0][:8])
            vendor_list.append(vend_name)

    else:
    ###For getting only AP vendors
        for row in packet_list[1:]:
            if "Beacon frame" in row[4]:
                device_list.append(str(row[0])[:])
                device_id = str(row[0])
                try:
                    vend_name, junk = device_id.split("_")
                except ValueError as e:
                    vend_name = str(row[0][:8])
                vendor_list.append(vend_name)

    #Get a count of how many packets from each vendor
    for item in vendor_list:
        if item not in vendor_packet_count:
            vendor_packet_count[item] = 1
        else:
            vendor_packet_count[item] += 1
    
    #print vendor_packet_count

    # Sort the vendor dictionary by count
    for key, val in vendor_packet_count.items():
        unique_vendor_list.append( (val, key) )
    unique_vendor_list.sort(reverse=True)
    # for val, key in unique_vendor_list:
    #     print key,val

    #Get a list of unique devices
    for item in device_list:
        if item not in device_packet_count:
            device_packet_count[item] = 1
        else:
            device_packet_count[item] += 1
    
    # Sort the device dictionary by count
    for key, val in device_packet_count.items():
        print key, val
        try:
            split_key, junk = key.split("_")
        except Exception as e:
            split_key = "Others"
        
        if split_key not in device_per_vendor:
            device_per_vendor[split_key] = 1
        else:
            device_per_vendor[split_key] += 1


    for key,val in device_per_vendor.items():
        if ":" in str(key[:4]) or val <INCLUDE_THRESH:
            if "Others" not in device_per_vendor:
                device_per_vendor["Others"]=0
                device_per_vendor[key] = 0
            else:
                device_per_vendor["Others"] += 1
                device_per_vendor[key] = 0
        
    if not INCLUDE_OTHERS:
        device_per_vendor["Others"] = 0


    for key, val in device_per_vendor.items():
            vendor_dist_dataset.append( (val, key) )
    vendor_dist_dataset.sort(reverse=False)


#Extract list of MAC addresses using API
def get_mac_list():
    for row in packet_list[1:]:
        if row[6] not in mac_list:
            mac_list.append(str(row[7]))
    for item in mac_list:
        time.sleep(0.05)
        vendor_name = get_vendor_name_api(item)
        if vendor_name not in device_per_vendor:
            device_per_vendor[vendor_name] = 1
        else:
            device_per_vendor[vendor_name] += 1
    
    for key, val in device_per_vendor.items():
            vendor_dist_dataset.append( (val, key) )

def plot_pie_chart(dataset):
    labels = []
    sizes = []
    for val, key in dataset:
        if val == 0:
            continue
        else:
            labels.append(key)
            sizes.append(val)
    colors = sns.color_palette("husl", 8)
    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, labels=labels, autopct=make_autopct(sizes),
            shadow=False, startangle=90, colors=colors)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

    plt.show()

def plot_bar_chart(dataset):
    vendor = []
    value = []
    for val, key in dataset:
        if val == 0:
            continue
        else:
            vendor.append(key)
            value.append(val)


    y_pos = np.arange(len(vendor))
    width = 0 

    fig, ax = plt.subplots()

    rects = ax.bar(y_pos, value, align='center', alpha = 0.8)
    ax.set_xticks(y_pos + width / 2)
    ax.set_xticklabels(vendor,fontdict = {'fontsize': 11})
    ax.set_ylabel('Number of devices', fontsize = 14)

    def autolabel(rects):
        """
        Attach a text label above each bar displaying its height
        """
        for rect in rects:
            height = rect.get_height()
            ax.text(rect.get_x() + rect.get_width()/2., 1.01*height,
                    '%d' % int(height),
                    ha='center', va='bottom',fontsize = 14)

    autolabel(rects)

    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    if len(sys.argv) > 1:
        try:
            file_name = str(sys.argv[1])
        except IOError as e:
            print("File not found.")

    read_csv(file_name)
    get_vendor_list(packet_list)
    #plot_pie_chart(vendor_dist_dataset)
    plot_bar_chart(vendor_dist_dataset)


    
