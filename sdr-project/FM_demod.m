function FM_demod(B1, N1, B2, N2, fs)
    %loading I & Q values from the signal (taken in 101.5 MHz FM radio)
    x_in = loadFile('capture.bin'); 

    %fs = 2400000; %sampling frequency
    fs2 = 48000;  %sampling rate
    %fc = 100e3;  %first LPF cutoff frequency
    %fc2 = 156e3; %second LPF cutoff frequency
    butterOrder = 3; %butterworth orderjusi
    %N1 = 10; %first decimation factor
    %N2 = 5;  %second decimation factor

    tau = 50e-6;
    %hold on;


    %simpleSA(x_in, 2^14, 2400); %displaying psd

    [b_lpf1, a_lpf1] = butter(butterOrder, 2*B1/fs); %butterworth filter

    %applying first low pass butterworth filter to the signal
    y_b1 = filter(b_lpf1, a_lpf1, x_in); 
    %simpleSA(y_b1, 2^14, 2400);

    %applying decimation to the filtered signal
    y_n1 = decimate(y_b1, N1);
    %simpleSA(y_n1, 2^14, 2400);

    %applying discriminator function
    z_dis = discrim(y_n1);
    %simpleSA(z_dis, 2^14, 2400);


    [b_lpf2, a_lpf2] = butter(butterOrder, 2*B2/fs); %butterworth filter
    %applying second low pass butterworth filter to the signal
    z_b2 = filter(b_lpf2, a_lpf2, z_dis); 
    %applying decimation to the filtered signal
    z_n2 = decimate(z_b2, N2);

    %rc lowpass 3db frequency
    f3 = 1/(2*pi*tau);
    a1 = exp(-2*pi*f3/fs);

    b_iir = [1-a1];
    a_iir = [1, -a1];

    z_out = filter(b_iir, a_iir, z_n2);
    %simpleSA(z_out, 2^14, 2400);
    sound(z_out,fs2);
end
